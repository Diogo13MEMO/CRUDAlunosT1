﻿namespace CRUDAlunosT1
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listBoxTodosAlunos = new System.Windows.Forms.ListBox();
            this.ComboBoxTodosAlunos = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TextBoxProcurar = new System.Windows.Forms.TextBox();
            this.buttonProcurar = new System.Windows.Forms.Button();
            this.ComboBoxProcurar = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TextBoxPrimeiroNome = new System.Windows.Forms.TextBox();
            this.TextBoxIdAluno = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxApelido = new System.Windows.Forms.TextBox();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonCancelar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listBoxTodosAlunos);
            this.groupBox1.Controls.Add(this.ComboBoxTodosAlunos);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(54, 46);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(341, 165);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Listagem de Alunos";
            // 
            // listBoxTodosAlunos
            // 
            this.listBoxTodosAlunos.FormattingEnabled = true;
            this.listBoxTodosAlunos.ItemHeight = 16;
            this.listBoxTodosAlunos.Location = new System.Drawing.Point(6, 51);
            this.listBoxTodosAlunos.Name = "listBoxTodosAlunos";
            this.listBoxTodosAlunos.Size = new System.Drawing.Size(217, 84);
            this.listBoxTodosAlunos.TabIndex = 1;
            // 
            // ComboBoxTodosAlunos
            // 
            this.ComboBoxTodosAlunos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxTodosAlunos.FormattingEnabled = true;
            this.ComboBoxTodosAlunos.Location = new System.Drawing.Point(6, 21);
            this.ComboBoxTodosAlunos.Name = "ComboBoxTodosAlunos";
            this.ComboBoxTodosAlunos.Size = new System.Drawing.Size(321, 24);
            this.ComboBoxTodosAlunos.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TextBoxProcurar);
            this.groupBox2.Controls.Add(this.buttonProcurar);
            this.groupBox2.Controls.Add(this.ComboBoxProcurar);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(431, 46);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(251, 165);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Procurar";
            // 
            // TextBoxProcurar
            // 
            this.TextBoxProcurar.Location = new System.Drawing.Point(128, 35);
            this.TextBoxProcurar.Name = "TextBoxProcurar";
            this.TextBoxProcurar.Size = new System.Drawing.Size(100, 22);
            this.TextBoxProcurar.TabIndex = 2;
            this.TextBoxProcurar.TextChanged += new System.EventHandler(this.TextBoxProcurar_TextChanged);
            // 
            // buttonProcurar
            // 
            this.buttonProcurar.Image = ((System.Drawing.Image)(resources.GetObject("buttonProcurar.Image")));
            this.buttonProcurar.Location = new System.Drawing.Point(128, 97);
            this.buttonProcurar.Name = "buttonProcurar";
            this.buttonProcurar.Size = new System.Drawing.Size(68, 47);
            this.buttonProcurar.TabIndex = 1;
            this.buttonProcurar.UseVisualStyleBackColor = true;
            this.buttonProcurar.Click += new System.EventHandler(this.button1_Click);
            // 
            // ComboBoxProcurar
            // 
            this.ComboBoxProcurar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxProcurar.FormattingEnabled = true;
            this.ComboBoxProcurar.Items.AddRange(new object[] {
            "Id Alunos",
            "Apelido"});
            this.ComboBoxProcurar.Location = new System.Drawing.Point(6, 33);
            this.ComboBoxProcurar.Name = "ComboBoxProcurar";
            this.ComboBoxProcurar.Size = new System.Drawing.Size(102, 24);
            this.ComboBoxProcurar.TabIndex = 0;
            this.ComboBoxProcurar.SelectedIndexChanged += new System.EventHandler(this.ComboBoxProcurar_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonCancelar);
            this.groupBox3.Controls.Add(this.buttonUpdate);
            this.groupBox3.Controls.Add(this.TextBoxApelido);
            this.groupBox3.Controls.Add(this.TextBoxPrimeiroNome);
            this.groupBox3.Controls.Add(this.TextBoxIdAluno);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(12, 253);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(747, 128);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Editar";
            // 
            // TextBoxPrimeiroNome
            // 
            this.TextBoxPrimeiroNome.Enabled = false;
            this.TextBoxPrimeiroNome.Location = new System.Drawing.Point(400, 29);
            this.TextBoxPrimeiroNome.Name = "TextBoxPrimeiroNome";
            this.TextBoxPrimeiroNome.Size = new System.Drawing.Size(115, 26);
            this.TextBoxPrimeiroNome.TabIndex = 4;
            // 
            // TextBoxIdAluno
            // 
            this.TextBoxIdAluno.Enabled = false;
            this.TextBoxIdAluno.Location = new System.Drawing.Point(147, 32);
            this.TextBoxIdAluno.Name = "TextBoxIdAluno";
            this.TextBoxIdAluno.ReadOnly = true;
            this.TextBoxIdAluno.Size = new System.Drawing.Size(116, 26);
            this.TextBoxIdAluno.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(521, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Apelido :";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(269, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Primeiro Nome";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID de Aluno :";
            // 
            // TextBoxApelido
            // 
            this.TextBoxApelido.Enabled = false;
            this.TextBoxApelido.Location = new System.Drawing.Point(606, 25);
            this.TextBoxApelido.Name = "TextBoxApelido";
            this.TextBoxApelido.Size = new System.Drawing.Size(135, 26);
            this.TextBoxApelido.TabIndex = 5;
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Enabled = false;
            this.buttonUpdate.Image = ((System.Drawing.Image)(resources.GetObject("buttonUpdate.Image")));
            this.buttonUpdate.Location = new System.Drawing.Point(366, 83);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(93, 36);
            this.buttonUpdate.TabIndex = 6;
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonCancelar
            // 
            this.buttonCancelar.Enabled = false;
            this.buttonCancelar.Image = ((System.Drawing.Image)(resources.GetObject("buttonCancelar.Image")));
            this.buttonCancelar.Location = new System.Drawing.Point(505, 83);
            this.buttonCancelar.Name = "buttonCancelar";
            this.buttonCancelar.Size = new System.Drawing.Size(110, 36);
            this.buttonCancelar.TabIndex = 7;
            this.buttonCancelar.UseVisualStyleBackColor = true;
            this.buttonCancelar.Click += new System.EventHandler(this.buttonCancelar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(771, 436);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "CRUD de Alunos";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox ComboBoxTodosAlunos;
        private System.Windows.Forms.ListBox listBoxTodosAlunos;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonProcurar;
        private System.Windows.Forms.ComboBox ComboBoxProcurar;
        private System.Windows.Forms.TextBox TextBoxProcurar;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox TextBoxPrimeiroNome;
        private System.Windows.Forms.TextBox TextBoxIdAluno;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxApelido;
        private System.Windows.Forms.Button buttonCancelar;
        private System.Windows.Forms.Button buttonUpdate;
    }
}

